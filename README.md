## installation

##### 1. git clone https://webhovhannisyan@bitbucket.org/webhovhannisyan/laravel-crud-test-task.git
##### 2. composer install
##### 3. Make .env file and add your settings
##### 4. php artisan migrate
##### 5. php artisan db:seed

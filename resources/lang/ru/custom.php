<?php

return [
    'ru' => 'Рус',
    'en' => 'Eng',
    'title' => 'Название',
    'description' => 'Описание',
    'logo' => 'Логотип',
    'add' => 'Добавить',
    'name' => 'Имя',
    'email' => 'Email',
    'password' => 'Пароль',
    'created_at' => 'Дата создания',
    'actions' => 'Действия',
    'edit' => 'Редактировать',
    'delete' => 'Удалить',
    'sections' => 'Отделы',
    'section' => 'Отдел',
    'users' => 'Пользователи',
    'user' => 'Пользователь',
    'nothing_found' => 'Ничего не найдено',
    'crud' => [
        'added' => ':entity успешно добавлен!',
        'deleted' => ':entity успешно удален!',
        'updated' => ':entity успешно обновлен!',
    ]
];

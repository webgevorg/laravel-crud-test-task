@extends('layouts.app')

@section('content')
    <div class="container">
        <div class="row justify-content-center">
            <div class="col-12 pt-4 pb-4 d-flex justify-content-between">
                <h4>{{ __('custom.users') }}</h4>
                <a class="btn btn-primary" href="{{ route('users.create') }}">{{ __('custom.add') }}</a>
            </div>
        </div>
        <div class="row justify-content-center">
            <div class="col-12">
                <table class="table">
                    <thead class="thead-dark">
                    <tr>
                        <th scope="col">#</th>
                        <th scope="col">{{ __('custom.name') }}</th>
                        <th scope="col">{{ __('custom.email') }}</th>
                        <th scope="col">{{ __('custom.created_at') }}</th>
                        <th scope="col">{{ __('custom.actions') }}</th>
                    </tr>
                    </thead>
                    <tbody>
                    @foreach($users as $user)
                        <tr>
                            <th scope="row">{{ $user->id }}</th>
                            <td>{{ $user->name }}</td>
                            <td>{{ $user->email }}</td>
                            <td>{{ $user->created_at }}</td>
                            <td>
                                <div class="d-flex">
                                    <a class="btn btn-warning mr-2" href="{{ route('users.edit', $user) }}">{{ __('custom.edit') }}</a>
                                    <form action="{{ route('users.destroy', $user) }}" method="post">
                                        @csrf
                                        {{ method_field('delete') }}
                                        <button class="btn btn-danger" type="submit">{{ __('custom.delete') }}</button>
                                    </form>
                                </div>
                            </td>
                        </tr>
                    @endforeach
                    </tbody>
                </table>

                {{ $users->links() }}
            </div>
        </div>
    </div>
@endsection

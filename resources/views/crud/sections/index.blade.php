@extends('layouts.app')

@section('content')
    <div class="container">
        <div class="row justify-content-center">
            <div class="col-12 pt-4 pb-4 d-flex justify-content-between">
                <h4>{{ __('custom.sections') }}</h4>
                <a class="btn btn-primary" href="{{ route('sections.create') }}">{{ __('custom.add') }}</a>
            </div>
        </div>
        <div class="row justify-content-center">
            <div class="col-12">
                @if($sections->isNotEmpty())
                    <table class="table">
                        <thead class="thead-dark">
                        <tr>
                            <th scope="col">#</th>
                            <th scope="col">{{ __('custom.logo') }}</th>
                            <th scope="col">{{ __('custom.title') }}</th>
                            <th scope="col">{{ __('custom.users') }}</th>
                            <th scope="col">{{ __('custom.created_at') }}</th>
                            <th scope="col">{{ __('custom.actions') }}</th>
                        </tr>
                        </thead>
                        <tbody>
                        @foreach($sections as $section)
                            <tr>
                                <th scope="row">{{ $section->id }}</th>
                                <td>
                                    <img class="crud-image" src="{{ '/storage/' . $section->logo ?? $section->default_logo }}" alt="">
                                </td>
                                <td>
                                    <div>{{ $section->name }}</div>
                                    <div>{{ $section->description }}</div>
                                </td>
                                <td>
                                    @if($section->users->isEmpty())
                                        {{ __('custom.nothing_found') }}
                                    @endif
                                    <ul>
                                        @foreach($section->users as $user)
                                            <li>{{ $user->name }}</li>
                                        @endforeach
                                    </ul>
                                </td>
                                <td>{{ $section->created_at }}</td>
                                <td>
                                    <div class="d-flex">
                                        <a class="btn btn-warning mr-2" href="{{ route('sections.edit', $section) }}">{{ __('custom.edit') }}</a>
                                        <form action="{{ route('sections.destroy', $section) }}" method="post">
                                            @csrf
                                            {{ method_field('delete') }}
                                            <button class="btn btn-danger" type="submit">{{ __('custom.delete') }}</button>
                                        </form>
                                    </div>
                                </td>
                            </tr>
                        @endforeach
                        </tbody>
                    </table>

                    {{ $sections->links() }}
                @else
                    <div class="text-center">{{ __('custom.nothing_found') }}</div>
                @endif
            </div>
        </div>
    </div>
@endsection

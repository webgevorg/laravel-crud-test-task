<?php

use App\User;
use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\Hash;

class UserSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $user = User::where('email', 'admin@test.loc')->first();

        if (! $user) {
            User::create([
                'name' => 'Admin',
                'email' => 'admin@test.loc',
                'password' => Hash::make('password')
            ]);
        }

        factory(App\User::class, 15)->create();
    }
}
